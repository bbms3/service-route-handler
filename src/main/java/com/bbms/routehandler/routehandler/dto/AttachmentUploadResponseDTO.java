package com.bbms.routehandler.routehandler.dto;

import java.io.Serializable;

public class AttachmentUploadResponseDTO implements Serializable {

    private String fileName;
    private Long uploadedSize;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getUploadedSize() {
        return uploadedSize;
    }

    public void setUploadedSize(Long uploadedSize) {
        this.uploadedSize = uploadedSize;
    }

}
