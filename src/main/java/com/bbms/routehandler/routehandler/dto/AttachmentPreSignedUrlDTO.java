package com.bbms.routehandler.routehandler.dto;

import java.io.Serializable;

public class AttachmentPreSignedUrlDTO implements Serializable {

    private String attachmentUrl;

    public String getAttachmentUrl() {
        return attachmentUrl;
    }

    public void setAttachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }
    
}
