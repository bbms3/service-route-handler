package com.bbms.routehandler.routehandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableJpaAuditing
@ComponentScan(basePackages = {"com.bbms.routehandler.*", "com.welzuka.*"})
@EnableJpaRepositories("com.bbms.routehandler.*")
@EntityScan("com.bbms.routehandler.routehandler.domain")
public class RouteHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RouteHandlerApplication.class, args);
	}

}